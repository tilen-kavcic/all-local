package space.AllLocal;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import im.delight.android.location.SimpleLocation;


public class MapActivity extends FragmentActivity implements GoogleMap.OnInfoWindowClickListener, OnMapReadyCallback{
    private GoogleMap mMap;

    private int farmeSt = 0;
    static final int LOCATION_SETTINGS_REQUEST = 1;

    public static double LAT = 0;
    public static double LON = 0;
    public static ArrayList<Marker> markerjiFarme = new ArrayList<>();
    private static final int requestPermissionID = 200;

    private final int[] articleImage = new int[]{R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4, R.drawable.image5, R.drawable.image6, R.drawable.image7, R.drawable.image8, R.drawable.image9, R.drawable.image10};

    private SimpleLocation location;

    final int min = 0;
    final int max = 9;
    Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        if (mapFragment != null) mapFragment.getMapAsync(MapActivity.this);

        location = new SimpleLocation(this);
        // if we can't access the location yet
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    makeToast();
                    startActivityForResult((new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)), 1);
                }
            }, 0);
        }

        // Get user location:
        try {
            LAT = location.getLatitude();
            LON = location.getLongitude();

        } catch (SecurityException e) {
            Toast.makeText(this, "We need your location to start the map.", Toast.LENGTH_SHORT).show();
            try {
                Thread.sleep(3000);                   // Wait for 3 Seconds
            } catch (Exception e2) {
                System.out.println("Error: " + e2);      // Catch the exception
            }
            restartApp();
        }

        // Log the location under 'USER LOCATION':
        Log.d("USER LOCATION", location.toString());

        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MapActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestPermissionID);
            return;
        }

        LinearLayout headerLinearLayout = findViewById(R.id.header);
        headerLinearLayout.bringToFront();

    }

    public void makeToast() {
        Toast.makeText(this, "We need your location to start the map.", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_SETTINGS_REQUEST) {
            // user is back from location settings - check if location services are now enabled
            if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(MapActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestPermissionID);
                return;
            }
            restartApp();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        location.beginUpdates();
    }

    @Override
    protected void onPause() {
        location.endUpdates();
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        try {
            // Customise the styling of the base map using a JSON object defined in a raw resource file.
            boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style));
            if (!success) {
                Log.e("MAPS_ACTIVITY", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MAPS_ACTIVITY", "Can't find style. Error: ", e);
        }

        LatLng MyLocation = new LatLng(LAT, LON);
        MarkerOptions marker = new MarkerOptions().position(MyLocation);

        showLocalFarmsFirst();

        // Changing marker icon
        marker.icon(vectorToBitmap(R.drawable.ic_map_my_location_on_map_icon));
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MyLocation, 10));
    }

    public void centrirajMe(View view) {
        LAT = location.getLatitude();
        LON = location.getLongitude();
        LatLng MyLocation = new LatLng(LAT, LON);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MyLocation, 10));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(this, FarmActivity.class);
        startActivity(intent);
    }

    public void showLocalFarmsFirst() {
        String[] names = new String[] {"Liam", "Emma", "Noah", "Olivia", "William", "Ava", "James", "Isabella", "Oliver", "Sophia", "Benjamin", "Charlotte", "Elijah", "Mia", "Lucas", "Amelia", "Mason", "Harper", "Logan", "Evelyn"};
        Random rand = new Random();
        for (int i = 0; i < 12; i++) {
            Double tempLat = LAT + (rand.nextDouble()*0.25 - 0.125);
            Double tempLng = LON + (rand.nextDouble()*0.25 - 0.125);
            Log.d("TRALALA", "LAT = " + LAT);
            Log.d("TRALALA", "LONG = " + LON);
            LatLng latLng = new LatLng(tempLat, tempLng);

            Marker farmMarker = mMap.addMarker(
                    new MarkerOptions()
                            .position(latLng)
                            .title(names[i] + "'s farm")
                            .snippet("★★★★☆")
                            .icon(vectorToBitmap(R.drawable.ic_map_farm_icon))
                            );
            farmMarker.setTag(0);
            markerjiFarme.add(farmMarker);
        }
        centrirajMe(null);
    }

    protected void showLocalFarms() {
        for (int i = 0; i < markerjiFarme.size(); i++) {
            markerjiFarme.get(i).setVisible(true);
        }
    }

    protected void hideLocalFarms() {
        for (int i = 0; i < markerjiFarme.size(); i++) {
            markerjiFarme.get(i).setVisible(false);
        }
    }

    public void toggleFarms(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate));
        if (farmeSt == 0) {
            showLocalFarms();
            farmeSt = 1;
        } else {
            hideLocalFarms();
            farmeSt = 0;
        }
    }

    private BitmapDescriptor vectorToBitmap(@DrawableRes int id) {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), id, null);
        assert vectorDrawable != null;
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        // DrawableCompat.setTint(vectorDrawable, color);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void openMap(View view) {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void openFarm(View view) {
        Intent intent = new Intent(this, FarmsListActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void openHelp(View view) {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }
    public void openMore(View view) {
        Intent intent = new Intent(this, MoreActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    private void restartApp() {
        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        assert i != null;
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    public void notAvailable(View view) {
        Toast.makeText(this, "Feature not available yet.", Toast.LENGTH_SHORT).show();
    }
}

