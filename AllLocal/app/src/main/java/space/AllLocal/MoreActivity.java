package space.AllLocal;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

public class MoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

    }

    public void openMap(View view) {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void openFarm(View view) {
        Intent intent = new Intent(this, FarmsListActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void openHelp(View view) {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }
    public void openMore(View view) {
        Intent intent = new Intent(this, MoreActivity.class);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void giveFeedback(View view) {
        String body = "#############" + "\n\n" +
                "SDK version: " + Build.VERSION.SDK_INT + "\n" +
                "Phone brand: " + Build.BRAND + "\n" +
                "Language: " + Locale.getDefault().toString() + "\n" +
                "#############" + "\n\n" +
                "Your suggestion" + "\n";

        String mail = "mad.salvaje@gmail.com";

        Log.d("SETTINGS ACTIVITY!!!", body);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mail, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback for All Local");
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(emailIntent, "Give feedback..."));
    }
}