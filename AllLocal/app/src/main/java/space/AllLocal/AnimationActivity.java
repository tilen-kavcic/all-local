package space.AllLocal;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;
//import android.widget.ViewAnimator;

import com.varunest.sparkbutton.SparkButton;

//import java.util.Map;

//import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

public class AnimationActivity extends FragmentActivity {

    private static final int requestPermissionID = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_activity);
        final SparkButton sparkButton = findViewById(R.id.spark_button);
        sparkButton.setEnabled(false);
        sparkButton.playAnimation();
        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(AnimationActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestPermissionID);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent startActivity = new Intent(AnimationActivity.this, MapActivity.class);
                    startActivity(startActivity);
                    finish();
                }
            }, 3000);
            return;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent startActivity = new Intent(AnimationActivity.this, MapActivity.class);
                startActivity(startActivity);
                finish();
            }
        }, 1900);


    }
}
